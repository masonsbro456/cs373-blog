---
layout: post
title:  "Week 5"
date:   2019-02-24 11:59:00 -0600
---
<b>What did you do this past week?</b>

This past week, my group got our project proposal approved, and we’re excited to get started.

I did a project for the Machine Learning and Data Science club that involved classifying Latin text as poetry or prose. I presented this project on Monday and listened to the presentations of others who attempted the same project.

I also helped run the third UT programming competition of the semester. The contest didn’t go as well as we’d hoped: two of the seven problems were not solved by any of the competitors. On the other hand, in an ideal contest, every problem gets solved by at least one person. Still, the contest didn’t have any major issues—all the problems were correct.

In between those things and general schoolwork, I solved competitive programming problems, went social dancing at the Fed, went to the gym, and played some volleyball.

<b>What's in your way?</b>

I need to meet with my group this week to discuss the division of labor for the project—until we decide who’s doing what, I can’t do very much!

<b>What will you do next week?</b>

I will hopefully start and finish my part of our first part of the group project.

My competitive programming team didn’t get a chance to practice this weekend, so we will try and find five hours this week to do that.

<b>What did you think of the talk by Ed on GCP and Hannah on AWS?</b>

Unfortunately I overslept on both of those days(!) and missed the talks.

<b>What's your pick-of-the-week or tip-of-the-week?</b>

My tip-of-the-week is an algorithmic technique known as square root decomposition. It comes up sometimes in competitive programming problems, and it is often a straightforward way to speed up a quadratic algorithm.

Say you have an array of integers, and you want to support two operations: an update and a query. For the update, you are supposed to increment or decrement all the numbers between two given indices L and R; for the query, you are to return the minimum number between two given indices L and R. If the array is of length N and you process M operations, the naïve approach leads to a time complexity of O(NM).

Instead, we split the array into N/B contiguous blocks of size B (a parameter we’ll decide later). For each block, we store the minimum number in that block, as well as all the values. Now to process an operation, when we iterate over the whole range we can take a shortcut when our interval covers a whole block: for the query, we can just look up the minimum; for the update, we can mark that we need to increment/decrement the actual values later, and update the summary number. We still have to do some work for the blocks that are only partially covered by the query/update, of course, so the time complexity of a single operation is O(B + N/B).

Picking B to be the square root of N, we get the time complexity of a sequence of M operations is O(N + M*sqrt(N)). This is a dramatic improvement! (This problem can be solved even faster, but this is a very simple and often good enough technique.) 
