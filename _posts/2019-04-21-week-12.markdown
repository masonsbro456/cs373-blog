---
layout: post
title:  "Week 12"
date:   2019-04-21 11:59:00 -0600
---
<b>What did you do this past week?</b>

This past week, I spent a lot of time on two things.

First, I worked a lot on searching, sorting, and filtering. I was pleased with the results my group ended up with and the design I ended up using for putting all those components together.

Second, I worked on organizing and writing a programming contest that will be held at UT on Saturday, May 4. This is different from the normal biweekly programming contests we host through ACM, and it will be aimed at students who want to participate in ICPC contests in the future. I wrote the contest with the help of Arnav Sastry, an alumnus of UTCS who also competed in ICPC in the past.

<b>What's in your way?</b>

Nothing’s in my way.

<b>What will you do next week?</b>

I will probably spend a lot of time working on a paper for another class on Monday, and then I will spend the rest of the week primarily finishing preparations for the May 4 programming contest (and the April 26 programming contest, which is a standard 2-hour contest on Friday night).

<b>What was your experience of Project #4: IDB3?</b>

Project 4 was one of the more interesting projects so far. There were interesting software engineering challenges in getting filtering, sorting, and searching to all work together well. I enjoyed thinking about these challenges, deciding on approaches and implementations, and then following through.

<b>What's your pick-of-the-week or tip-of-the-week?</b>

My pick-of-the-week is <a href="https://www.elephantsql.com/">ElephantSQL</a>. It is a managed database hosting service specializing in Postgres, and my group is using it for our project. There are many database hosting services out there (including AWS and GCP), but ElephantSQL was extraordinarily simple to set up, and provides all the functionality we needed in a clean interface. It probably isn’t suitable for very large applications or enterprise organizations like AWS and GCP are, but for our purposes it has been excellent. There is a free tier with enough storage and other resources for a small project, but we found it wasn’t quite enough for us: we needed more than the allowed 5 concurrent database connections in order to have the site run in production and also locally for development, while having both connect to the production database. (An alternative solution would have been to use a different database for development, but we found it was easier to just pay $5 for the 10-concurrent-conection upgrade!)
