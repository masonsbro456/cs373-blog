---
layout: post
title:  "Week 3"
date:   2019-02-10 11:59:00 -0600
---
<b>What did you do this past week?</b>

I had a pretty relaxed week. On Thursday I went to a Texas Swing Dance Society meeting, which was fun—I don’t know if I’ll keep going, though. On Friday I helped run the second UT programming contest of the semester. On Saturday my ICPC competitive programming team did a 5-hour practice contest; disappointingly, we didn’t do as well as we could have.

Throughout the week I’ve been doing problems from Project Euler. I first made an account on that website a few years ago, but it’s been a while since I used it regularly—it’s fun to get back into it!

<b>What's in your way?</b>

Nothing’s in my way.

<b>What will you do next week?</b>

This week, I have to do the Collatz project and review a paper for another class. I hope to do more competitive programming and Project Euler problems in my free time.

<b>What was your experience of learning the basics of Python?</b>

There are some counterintuitive things in Python that I wouldn’t have expected. For example, since tuples are immutable it makes sense that making a copy is unnecessary, but it is strange that even calling the copy function on a tuple returns the original! I would have expected the copy function to be agnostic to the type of the argument and to always return an actual copy. 

<b>What's your pick-of-the-week or tip-of-the-week?</b>

My pick-of-the-week is [Project Euler](https://projecteuler.net). It is a site with a collection of about 650 math and programming problems of various difficulties: the first problem is a variant of the popular interview question FizzBuzz, and there are many hard questions as well. Most of the questions are math-heavy, but require small programs to actually find the answers. Solving problems on this site is a great way to practice writing correct code quickly. There’s even a [problem](https://projecteuler.net/problem=14) about the Collatz conjecture!

If you like competition, there are virtual awards you can get for things like solving a certain number of problems and being among the first 100 people to solve a problem. I hope one day to be the first to solve one of the problems, but that’s a hard thing to do—the most recent problem was solved by someone in under 7 minutes!
